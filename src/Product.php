<?php

namespace App;

class Product
{
    public function __construct(public null|string $name = null)
    {
    }
}