<?php

use App\Boutique;
use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    public function testTrue()
    {
        $this->assertTrue(true);
    }

    public function testHorsService()
    {
        $this->assertTrue(true);
    }

    /**
     * Add product to list
     */
    public function testAddProduct()
    {
        $boutique = new Boutique();
        $products = $boutique->products;
        $this->assertCount(0, $products);
        $boutique->addProduct('Coca');
        $products = $boutique->products;
        $this->assertCount(1, $products);
    }
}
